const Sequelize = require("sequelize");
const UserModel = require("./models/user");
const newsModel = require("./models/news");
const addModel  = require("./models/add")

const sequelize = new Sequelize(
  "newsapp",
  "root",
  "Ashi@7989",
  {
    host: "localhost",
    dialect: "mysql",
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  }
);

const User = UserModel(sequelize, Sequelize);
const News = newsModel(sequelize, Sequelize);
const Adds = addModel(sequelize, Sequelize);

sequelize.sync().then(() => {
  console.log(`Database & tables created!`);
});

module.exports = {
  User,
  News,
  Adds
};
