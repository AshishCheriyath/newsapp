module.exports = (sequelize, type) => {
    return sequelize.define('news', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        heading: type.STRING,
        description:type.STRING,
        short_description:type.STRING,
        image_url:type.STRING,
        type:type.STRING,
        isHighLight: {
          type: type.INTEGER,
          defaultValue: '0'
        }
        ,
        isLatest: {
          type: type.INTEGER,
          defaultValue: '0'
        },

    })
}