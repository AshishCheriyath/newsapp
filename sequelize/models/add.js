module.exports = (sequelize, type) => {
    return sequelize.define('add', {
        id: {
          type: type.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        url: type.STRING,
        description:type.STRING,
        image_url:type.STRING,
        type:type.STRING
    })
}