const { User }              = require("../sequelize/sequelize");
const randomStringGenerator = require("../services/randomStringGenerator");
const tokenService          = require("../services/tokenService");

module.exports.userLogin = async(username,password)=>{
    try {
        let userLoggedData = await User.findOne({where:{email:username}},{plain:true});
        if(userLoggedData.dataValues && userLoggedData.dataValues.password == password){
            let randomString = await randomStringGenerator.generateRandomString(5);
            //update user with newly generated token
            //generate jwt token
            //send jwt token
            let updatedUserToken  = await User.update({token:randomString},{where:{email:username}});
            let generatedJwtToken = await tokenService.createJwtToken(userLoggedData.dataValues.id);
            return generatedJwtToken;
        }
        else{
            throw new Error("UserName ANd Password Donot match");
        }
    } catch (error) {
        console.log(error);
        throw new Error("Error in Login USing Username and password");
    }
}