const { News } = require('../sequelize/sequelize');
const s3FileUpload = require("../services/s3FileUpload")
module.exports.getNewsData = async(newsType,limit,skip)=>{
    try {
        let newsData = await News.findAll({ limit,
    skip,where:{type:newsType},order: [
        ['createdAt', 'DESC']]},{plain:true});
        return (newsData.length > 0 ?newsData :[]);
    } catch (error) {
        console.log(error);
        throw new Error("Error in Fetching News Data");
    }
}


module.exports.getAllNewsData = async()=>{
    try {
        let newsData = await News.findAll({order: [
            ['createdAt', 'DESC']]},{plain:true});
        //console.log("newsData",newsData)
        return (newsData.length > 0 ?newsData :[]);
    } catch (error) {
        console.log(error);
        throw new Error("Error in Fetching News Data");
    }
}


module.exports.findIndividualNewsData = async(newsId)=>{
    try {
        let individulaNewsData = await News.findOne({where:{id:newsId}});
        return individulaNewsData;
        
    } catch (error) {
        console.log(error);
        return res.status(500).json({"status":false,"msg":"Error in Fetching News Data"})
    }
}



module.exports.addNewsData = async(newsData,filepath)=>{
    try {
        //let newsObject = {};
        let s3UploadedPath = await s3FileUpload.s3FileUpload(filepath);
        newsData.image_url = s3UploadedPath.data.Location;
        let addedNewsData = await News.create(newsData);
        return newsData;
        
    } catch (error) {
        console.log(error);
        throw new Error("Error in Adding News Data");
        
    }
}

module.exports.findLatestNews = async()=>{
    try {
        let latestNews =  await News.findAll({where:{isLatest:1},order: [
            ['createdAt', 'DESC']]});
        return latestNews;
        
    } catch (error) {
        console.log(error)
        throw new Error("Error in finding Latest News Data");
    }
}

module.exports.findHighlightedNews = async()=>{
    try {
        let highlightedNews =  await News.findAll({where:{isHighLight:1},order: [
            ['createdAt', 'DESC']]});
        return highlightedNews;
        
    } catch (error) {
        console.log(error)
        throw new Error("Error in finding Latest News Data");
    }
}

module.exports.deleteNews = async(newsId)=>{
    try {
        let highlightedNews =  await News.destroy({
            where: {
                // criteria
                id:newsId
            }
        })
        return highlightedNews;
        
    } catch (error) {
        console.log(error)
        throw new Error("Error in finding Latest News Data");
    }
}