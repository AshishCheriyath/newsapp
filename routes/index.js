var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/addNews', function(req, res, next) {
  res.render('news/addNews', { title: 'Express' });
});

module.exports = router;
