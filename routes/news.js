var express           = require('express');
var router            = express.Router();
const newsController  = require("../controllers/newsController");
const multerService   = require("../services/multer");
const tokenService    = require("../services/tokenService");

/* GET users listing. */
router.get('/getNews/:newsType',newsController.getNewsFeeds);

router.get('/getIndividualNews/:newsid',newsController.getIndividualNews);

router.get('/getAllNews',newsController.getAllNewsData)

router.get('/getNewsHighlights',newsController.getNewsHighlighted)

router.get('/getLatestNews',newsController.getLatestNews)

router.post('/addNews',tokenService.validateToken,function (req, res, next) {
    multerService.imageUpload(req, res, function (err) {
        if (err) {
            console.log(err)
            if (err.type == "file_type_error") {
                return res.status(422).json({ "result": "error", "msg": "Invalid File Extension,Please Select File Type png, jpg, jpeg, gif "})
            }
            else if (err.code == "LIMIT_FILE_SIZE") {
                return res.status(413).json({"result": "error", "msg": "File too large, you can upload files up to 2 MB"})
            }
            else{
                return res.status(400).json({"result": "error", "msg": "UnExpected File Upload Error"})
            }
        }
        else{
            next();
        }
        
    });
},newsController.addNews);

router.post('/removeNews',newsController.removeNews)

module.exports = router;
