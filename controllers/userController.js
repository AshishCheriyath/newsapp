const userRepo = require("../repositories/userRepository");
module.exports.userLogin =async(req,res)=>{
    try {
        let username = req.body.username ? req.body.username : "";
        let password = req.body.password ? req.body.password : "";
        if(username == "" || password == ""){
            return res.status(422).json({"status":false,"msg":"User Name or Password Missing"});
        }
        else{
            let userLoginData = await userRepo.userLogin(username,password);
            return res.status(200).json({"status":true,"msg":"user Logined Successfully","jwtToken":userLoginData})
        }
        
    } catch (error) {
        console.log(error);
        return res.status(500).json({"status":false,"msg":"Error In User Login"});
    }
}