const newsRepository = require("../repositories/newsRepository");

module.exports.getNewsFeeds = async(req,res)=>{
    try {
        let newsType = req.params.newsType ? req.params.newsType: ""; 
        let limit = parseInt(req.query.limit);
        let skip = parseInt(req.query.skip);
        if(newsType ==""){
            return res.status(422).json({"status":false,"msg":"Missing News Type"});
        }
        else{
            let newsFetched = await newsRepository.getNewsData(newsType,limit,skip);
            let newsData = newsFetched.length >0 ? newsFetched: [];
            return res.status(200).json({"status":true,"msg":"Fetched News Data","data":newsData});
        }
        
    } catch (error) {
        console.log(error);
        return res.status(500).json({"status":false,"msg":"Error in Fetching News Feeds"});
        
    }
}


module.exports.addNews = async(req,res)=>{
    try {
        //console.log(req.file)
        let jsonObject = {};
        let parsedData = JSON.parse(req.body.data);
        // parsedData.forEach(function (data) {
        //     jsonObject[data.name] = data.value;
        // })

        for(let i=0; i<parsedData.length; i+=1){
            // parsedData.forEach(function (data) {
                const data =parsedData[i];
                jsonObject[data.name] = data.value;
            // })
        }

        (jsonObject.isHighlight == true)?jsonObject.isHighLight=1:jsonObject.isHighLight=0;
        (jsonObject.isLatest == true)?jsonObject.isLatest=1:jsonObject.isLatest=0;
        
        let newNews = await newsRepository.addNewsData(jsonObject,req.file.filename);
        return res.status(200).json({"status":true,"msg":"Added News"})
        
    } catch (error) {
        console.log(error);
        return res.status(500).json({"status":false,"msg":"Error in Adding News"})
        
    }
}


module.exports.getAllNewsData = async(req,res)=>{
    try {
        let allNewsData = await newsRepository.getAllNewsData();
        return res.status(200).json({"status":true,"msg":"Fetched News Data","data":allNewsData});
        
    } catch (error) {
        console.log(error);
        return res.status(500).json({"status":false,"msg":"Error In fetching Data"});
        
    }
}


module.exports.getIndividualNews = async(req,res)=>{
    try {

        let newsId =  req.params.newsid;
        let newsData = await newsRepository.findIndividualNewsData(newsId)
        return res.status(200).json({"status":true,"msg":"Fetched News Data","data":newsData});
        
    } catch (error) {
        console.log(error);
        return res.status(500).json({"status":false,"msg":"Error In fetching Data"});
        
    }
}

module.exports.getLatestNews = async(req,res)=>{
    try {
        let LatestNewsData = await newsRepository.findLatestNews();
        return res.status(200).json({"status":true,"msg":"Fetched latest News Data","data":LatestNewsData});
        
    } catch (error) {
        console.log(error)
        return res.status(500).json({"status":false,"msg":"Error In getting Latest News"});
    }
}

module.exports.getNewsHighlighted = async(req,res)=>{
    try {
        let highLightedNews = await newsRepository.findHighlightedNews();
        return res.status(200).json({"status":true,"msg":"Fetched latest News Data","data":highLightedNews});
        
    } catch (error) {
        console.log(error)
        return res.status(500).json({"status":false,"msg":"Error In getting Highlighted News"});
    }
}


module.exports.removeNews = async(req,res)=>{
    try {
        let newsId = req.body.newsid;
        let deletedNewsData = await newsRepository.deleteNews(newsId);
        return res.status(200).json({"status":true,"msg":"Deleted News"});
        
    } catch (error) {
        console.log(error);
        return res.status(500).json({"status":false,"msg":"Error in Removing News"})
    }
}