const aws = require('aws-sdk');
const multerS3 = require('multer-s3');
const multer = require('multer');
var fs = require('fs');
const deleteFile = require("../services/deleteUploadedFile");

aws.config.update({
    secretAccessKey: "5OOeXWouvNXpnzEsFRLymjQLmdZtinRkzXUnAUwz",
    accessKeyId: "AKIAIZTFCBPQKLJMLWGA",
    region: "ap-south-1"
});


let s3 = new aws.S3();


class fileUpload {

    async s3FileUpload(fileName) {
        try {
            const rs = fs.createReadStream('./uploads/' + fileName);
            // console.log("rs",rs)
            rs.on('open', () => {
                console.log('OPEN')
            })
            rs.on('end', () => {
                console.log('END')
            })
            rs.on('close', () => {
                console.log('CLOSE')
            })

            const response = await s3.upload({
                Bucket: "varthamalayalamnewbucket",
                Key: 'newsImages/' + fileName,
                Body: rs,
            }).promise();
            console.log("response",response)
            let deleteUploadedFile = await deleteFile.deleteUploadedFile(fileName);
            return ({ "status": 200, "response": "succes", "data": response })

        } catch (err) {
            console.log(err);
            return ({ "status": 400, "response": "error", "message": "Error in s3 file upload" })
        }
    }

    async deleteS3Image(filename) {
        try {

            var res = filename.split("https://ucodifybucket.s3.us-east-2.amazonaws.com/");
            let awsFile = res[1].toString();
            console.log("File name to delete:", awsFile);
            var params = {
                Bucket: CONFIG.BUCKET,
                Key: awsFile
            };
            let deletedStatus = await s3.deleteObject(params).promise();
            return ({ "status": 200, "response": "succes", "data": deletedStatus })

        } catch (err) {
            console.log(err);
            return ({ "status": 400, "response": "error", "data": err })
        }

    }

    async checkFileExsistInS3(fileName) {
        var res = fileName.split("https://ucodifybucket.s3.us-east-2.amazonaws.com/");
        if (res[1]) {
            let awsFile = res[1].toString();
            const params = {
                Bucket: CONFIG.BUCKET,
                Key: awsFile  //"filename" //if any sub folder-> path/of/the/folder.ext
            }
            try {
                let awsFileStatus = await s3.headObject(params).promise();
                return true;
            } catch (err) {
                console.log("err")
                return false;
            }

        }
        else {
            return false;
        }

    }

    async deleteS3Folder(folder) {
        console.log("Folder name:", folder);
        try {
            const listParams = {
                Bucket: CONFIG.BUCKET,
                Prefix: folder
            };

            const listedObjects = await s3.listObjectsV2(listParams).promise();

            if (listedObjects.Contents.length === 0) {
                return ({ "status": 200, "response": "succes", "data": "Deletion successful!" });
            }

            const deleteParams = {
                Bucket: CONFIG.BUCKET,
                Delete: { Objects: [] }
            };

            listedObjects.Contents.forEach(({ Key }) => {
                deleteParams.Delete.Objects.push({ Key });
            });

            await s3.deleteObjects(deleteParams).promise();

            if (listedObjects.IsTruncated)
                await deleteS3Folder(folder);
            return ({ "status": 200, "response": "succes", "data": "Deletion successful!" });

        } catch (err) {
            console.log(err);
            return ({ "status": 400, "response": "error", "data": err });
        }
    }
}

module.exports = new fileUpload();