var express = require("express");
var multer = require('multer');
var fs = require('fs');
var path = require('path')

var storage = multer.diskStorage({
  destination: function (req, file, cb) {

    var dir = './uploads';

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, makeid(5)+Date.now().toString())
  }
})

// 
var upload = multer({ //multer settings
  storage: storage,
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if (ext !== '.xlsx' && ext !== '.xls') {
      return callback(new Error(`Could Not Write File.Invalid File Type.`))
    }
    callback(null, true)
  },
  limits: {
    fileSize: 1024 * 1024 * 1
  }
}).single('files');


var imageUpload = multer({ //multer settings
  storage: storage,
  fileFilter: function (req, file, callback) {
    var ext = path.extname(file.originalname);
    if(ext !== '.PNG' && ext !== '.png' && ext !== '.JPG' && ext !== '.jpg' && ext !== '.gif' && ext !== '.GIF' && ext !== '.jpeg' && ext !== '.JPEG') {
      return callback({"type":"file_type_error","msg":"Could Not Write File.Invalid File Type."})
    }
    callback(null, true)
  },
  limits: {
    fileSize: 1024 * 1024 * 8
  }
 }).single('file')
//}).fields([{ name: 'imgfiles', maxCount: 5 },{ name: 'quesImg', maxCount: 5 }])



var profileImageUpload = multer({ //multer settings
  storage: storage,
  fileFilter: function (req, file, callback) {
      
      var ext = path.extname(file.originalname);
      if(ext !== '.png' && ext !== '.PNG' && ext !== '.jpg' && ext !== '.JPG'  && ext !== '.jpeg' && ext !== '.JPEG' && ext !== '.svg'  && ext !== '.SVG' ) {
          return callback(new Error(`Could Not Write File.Invalid File Type.`))
      }
      callback(null, true)
  },
  limits:{
      fileSize: 1024 * 1024 * 2
  }
}).single('file');


function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}






module.exports.upload = upload;
module.exports.imageUpload = imageUpload;
module.exports.profileImageUpload = profileImageUpload;

