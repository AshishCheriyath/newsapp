const jwt = require('jsonwebtoken');
module.exports.createJwtToken = async(userId)=>{
    try {
        var token = jwt.sign({id: userId},'varthanews' , {
            expiresIn: 86400 // expires in 24 hours
        });
        return token;
    } catch (error) {
        console.log(error);
        throw new Error("Error in token creation");
    }
};

module.exports.validateToken =async(req,res,next)=>{
    try {
        var token = req.headers['x-access-token'];
        var msg = { auth: false, message: 'No token provided.' };
        if (!token) return res.status(500).send(msg);
        jwt.verify(token, 'varthanews', function (err, decoded) {
            var msg = { auth: false, message: 'Failed to authenticate token.' };
            if (err) return res.status(500).send(msg);
            next();
        });
        
    } catch (error) {
        console.log(error);
        throw new Error("Error in token validation");
    }
}



