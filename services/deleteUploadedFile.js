
const path = require('path');
const fs = require('fs');

module.exports.deleteUploadedFile = async (filename) => {
    try {
        // console.log(filename,type)//
        let paths = path.join(__dirname, '..', 'uploads', filename);;
        // delete file named 'sample.txt' Synchronously
        let deletedStatus = await fs.unlinkSync(paths);
        return ({ "result": "success", "data": "deletedStatus" })
    } catch (err) {
        console.log(err)
        return ({ "result": "error", "data": err })

    }
}